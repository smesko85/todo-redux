import reducer, { actions, actionTypes } from './reducer';

it('should have proper exports', () => {
  expect(typeof reducer).toBe('function');
  expect(typeof actions).toBe('object');
  expect(typeof actionTypes).toBe('object');
});

it('should have proper action types', () => {
  expect(actionTypes.ADD_TODO).toBe('ADD_TODO');
  expect(actionTypes.UPDATE_TODO).toBe('UPDATE_TODO');
  expect(actionTypes.TOGGLE_TODO).toBe('TOGGLE_TODO');
  expect(actionTypes.REMOVE_TODO).toBe('REMOVE_TODO');
});

describe('actions', () => {
  it('should have add todo action', () => {
    const description = `Todo ${Date.now()}`;
    const firstAction = actions.add(description);
    expect(firstAction.type).toBe(actionTypes.ADD_TODO);
    expect(firstAction.payload.id).toBeTruthy();
    expect(firstAction.payload.description).toBe(description);
    expect(firstAction.payload.completed).toBe(false);

    const secondAction = actions.add('Another one');
    expect(secondAction.payload.id).not.toBe(firstAction.payload.id);
  });

  it('should have remove todo action', () => {
    const id = Date.now();
    const action = actions.remove({ id });
    expect(action.type).toBe(actionTypes.REMOVE_TODO);
    expect(action.payload.id).toBe(id);
  });

  it('should have toggle action', () => {
    const firstAction = actions.toggle({ id: 1, completed: false });
    expect(firstAction.type).toBe(actionTypes.TOGGLE_TODO);
    expect(firstAction.payload.id).toBe(1);
  });

  it('should have update action', () => {
    const description = `Updated ${Date.now()}`;
    const action = actions.update({ id: 1, description });
    expect(action.type).toBe(actionTypes.UPDATE_TODO);
    expect(action.payload.description).toBe(description);
  });
});

describe('reducer', () => {
  it('should return [] as initial values', () => {
    expect(reducer(undefined, {})).toEqual([]);
  });

  it('should handle add todo', () => {
    const description1 = `Todo ${Date.now()}`;
    const state1 = reducer([], actions.add(description1));
    expect(state1.length).toBe(1);
    expect(state1[0].description).toBe(description1);

    const description2 = `Another Todo ${Date.now()}`;
    const state2 = reducer(state1, actions.add(description2));
    expect(state2.length).toBe(2);
    expect(state2[1].description).toBe(description2);
  });

  it('should handle remove todo', () => {
    expect(reducer([], actions.remove({ id: 1 }))).toEqual([]);
    expect(
      reducer(
        [{ id: 1, description: 'todo', completed: false }, { id: 2, description: 'todo2', completed: true }],
        actions.remove({ id: 1 }),
      ),
    ).toEqual([{ id: 2, description: 'todo2', completed: true }]);
  });

  it('should handle update todo', () => {
    expect(reducer([{ id: 1, description: 'todo', completed: false }], actions.update({ id: 1, description: 'test' }))).toEqual([
      { id: 1, description: 'test', completed: false },
    ]);
  });

  it('should handle toggle todo', () => {
    expect(reducer([{ id: 1, description: 'todo', completed: false }], actions.toggle({ id: 1 }))).toEqual([
      { id: 1, description: 'todo', completed: true },
    ]);
  });
});
